<!DOCTYPE html>
<html>
<head>
	<title>CRUD Struktur Organisasi</title>
</head>
<body>

	<h3>Edit Data Company</h3>

	@foreach($company as $p)
	<form action="/company/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $p->id }}">
		Nama<br/> <input type="text" required="required" name="nama" value="{{ $p->nama }}"> <br/>
		Alamat <br/><input type="text" required="required" name="alamat" value="{{ $p->alamat }}"> <br/>
		<br/>
		<input type="submit" value="Simpan Data">
	</form>
    <br/>
    <a href="/company"><button>Kembali</button></a>
	@endforeach
		

</body>
</html>