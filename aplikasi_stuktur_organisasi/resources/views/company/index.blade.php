<!DOCTYPE html>
<html>
<head>
	<title>CRUD Struktur Organisasi</title>
</head>
<body>
	<h3>Data Company</h3>

	<a href="/company/tambah"><button>Tambah Company Baru</button></a>
	
	<br/>
	<br/>

	<table border="3">
		<tr>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Opsi</th>
		</tr>
		@foreach($company as $p)
		<tr>
			<td>{{ $p->nama }}</td>
			<td>{{ $p->alamat }}</td>
			<td>
				<a href="/company/edit/{{ $p->id }}">Edit</a>
				|
				<a href="/company/hapus/{{ $p->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>


</body>
</html>