<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function index()
    {
    	$employee = DB::table('employee')->get();
 
    	return view('index',['employee' => $employee]);
 
    }

    public function tambah()
    {
        // memanggil view tambah
        return view('tambah');
    
    }

    public function store(Request $request)
    {
        // insert data ke table employee
        DB::table('employee')->insert([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan,
            'company_id' => $request->company
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/employee');
    
    }

    public function edit($id)
    {
        // mengambil data employee berdasarkan id yang dipilih
        $employee = DB::table('employee')->where('id',$id)->get();
        // passing data employee yang didapat ke view edit.blade.php
        return view('edit',['employee' => $employee]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('employee')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan,
            'company_id' => $request->company
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/employee');
    }

    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('employee')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/employee');
    }

}
