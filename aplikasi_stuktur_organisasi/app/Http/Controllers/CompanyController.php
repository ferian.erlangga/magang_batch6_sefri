<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index()
    {
    	$company = DB::table('company')->get();
 
    	return view('company/index',['company' => $company]);
 
    }

    public function tambah()
    {
        // memanggil view tambah
        return view('company/tambah');
    
    }

    public function store(Request $request)
    {
        // insert data ke table employee
        DB::table('company')->insert([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);

        return redirect('/company');
    
    }

    public function edit($id)
    {
        // mengambil data employee berdasarkan id yang dipilih
        $company = DB::table('company')->where('id',$id)->get();
        // passing data employee yang didapat ke view edit.blade.php
        return view('company/edit',['company' => $company]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('company')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/company');
    }

    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('company')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/company');
    }
}
