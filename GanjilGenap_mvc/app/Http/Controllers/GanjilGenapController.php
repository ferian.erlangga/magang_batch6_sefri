<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GanjilGenapController extends Controller
{

    public function index()
    {
        return view('GanjilGenap');
    }
    public function aksi(Request $request)
    {
        $a = $request->a;
		$b = $request->b;
		for ($i = $a; $i <= $b; $i++) {
            if ($i % 2 == 0) $hasil = "Angka " . $i . " adalah genap<br>";
            else $hasil = "Angka " . $i . " adalah ganjil<br>";
        }
        return view('GanjilGenap', compact('hasil'));
    }
}
